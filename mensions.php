<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <title>Accueil Webmaster et Graphisme</title>
    <link rel="stylesheet" href="css/normalize.css" type="text/css" />
    <link rel="stylesheet" href="css/style.css" type="text/css" />
    <link rel="stylesheet" href="css/blog-mobile_style.css" type="text/css" />



    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Webdesign et Graphisme à Carcassonne et dans l'Aude">

    <link href="https://fonts.googleapis.com/css?family=Pacifico|Playfair+Display:400,400i,700i|Raleway:400,400i,700i" rel="stylesheet">
    <link rel="shortcut icon" href="favicon.ico" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.css" />

    <!--[if lt IE 9]>
<script
src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js">
</script>
<![endif]-->

</head>

<body>

    <header>

        <nav id="menu" class="js-scrollTo">
            <ul class="flex js-scrollTo">
                <li><a class="js-scrollTo" href="index.php#print">Print</a></li>
                <li><a class="js-scrollTo" href="index.php#web">Web</a></li>
                <li><a class="js-scrollTo" href="index.php#contact">Contact</a></li>
            </ul>
        </nav>

        <section id="montitre">

            <div class="titre1"> Hristo Apostolov</div>
            <div class="titre2"> Webdesign et Graphisme</div>

        </section>



        <section id="entete">

            <div class="titre1"> Hristo Apostolov</div>
            <div class="titre2"> Webdesign et Graphisme</div>

        </section>

        <nav id="menu-mobile">
            <ul>
                <li><a class="js-scrollTo" href="#print">Print</a></li>
                <li><a class="js-scrollTo" href="#web">Web</a></li>
                <li><a class="js-scrollTo" href="#contact">Contact</a></li>
            </ul>
        </nav>
        <!-- fin menu mobile -->



    </header>

    <main class="conteneur">
<div>
      <p class="mensionsLegales">Mentions légales</p>
<p>En vertu de l’article 6 de la loi n° 2004-575 du 21 juin 2004 pour la confiance dans l’économie numérique, il est précisé aux utilisateurs du site www.hristo-portfolio.alwaysdata.net l’identité des différents intervenants dans le cadre de sa réalisation et de son suivi :</p>

<p><span class="mensionsLegales">Propriétaire :</span> Hristo Apostolov - 11000 Carcassonne - France hristo.apostolov@yahoo.com</p>

<p><span class="mensionsLegales">Créateur :</span>  Hristo Apostolov</p>

<p><span class="mensionsLegales"> Responsable publication :</span> Hristo Apostolov</p>

<p><span class="mensionsLegales">Webmaster :</span>  Hristo Apostolov</p>

<p><span class="mensionsLegales">Hébergeur :</span>  LWS - 4 Rue Galvani - 75838 Paris - Cedex 17 - FRANCE</p>

<p><span class="mensionsLegales">Crédit photo :</span>  Hristo Apostolov</p>

<p><span class="mensionsLegales">Crédit photo :</span>  Freepik et ses différents auteurs contributeurs.</p>

<p>Ce site est conforme au <span class="mensionsLegales">RGPD</span> (Règlement Général sur la Protection des Données) RÈGLEMENT du PARLEMENT EUROPÉEN (UE) 2016/679 et du CONSEIL de l'EUROPE du 27 avril 2016 relatif à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données.</p>

</div>

    </main>

    <footer>
        <div class="flex conteneur">
            <div class="mentions">
                <p class="LogoFooter"> &copy; Hristo Apostolov Créations </p>
                <p class="telFooter">06 22 91 42 07 </p>
            </div>
            <div class="credits">
                <nav id="pied">
                    <ul class="flex">
                        <li><a class="js-scrollTo" href="#print">Print</a></li>
                        <li><a class="js-scrollTo" href="#web">Web</a></li>
                        <li><a class="js-scrollTo" href="#contact">Contact</a></li>
                        <li>
                            <a href="https://www.instagram.com/designedbyhristo"><i class="fab fa-instagram"></i></a></li>

                        <li> <a href="https://www.linkedin.com/in/hristo-apostolov-92aa8929/"><i class="fab fa-linkedin-in"></i></a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </footer>

    <!-- DEBUT JAVA SCRIPT -->

    <!-- Inclusion bibliothèque Jquery -->
    <script src="js/jquery-1.12.4.min.js"></script>
    <!-- Inclusion feuille de scrpit perso -->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.js">
    </script>

    <script type="text/javascript">
        $(function() {
            $(window).scroll(function() {
                var scroll = $(window).scrollTop();
                if (scroll >= 10) {
                    $(".fondmenu").addClass("fixe");

                } else {
                    $(".fondmenu").removeClass("fixe");

                }

            });

            $('.js-scrollTo').on('click', function() { // Au clic sur un élément
                var page = $(this).attr('href'); // Page cible
                var speed = 750; // Durée de l'animation (en ms)
                $('html, body').animate({
                    scrollTop: $(page).offset().top
                }, speed); // Go
                return false;
            });
        });

    </script>

    <!-- FIN JAVA SCRIPT -->
    <script src="js/zone_script.js" type="text/javascript"></script>

</body>

</html>
