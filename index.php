<?php
require_once('projets.php');

//Init variables
$resultats = array();

$type = (isset($_GET['type'])) ? $_GET['type'] : '' ;


if($_SERVER["REQUEST_METHOD"] == "GET"){

  //Filtre par ville
  if(!empty($type)){
    foreach($projets as $k => $annonce){
      if($type == $annonce['type']){
        $resultats[] = $annonce;
      }
    }
  } else {
    $resultats = $projets;
  }
  }
?>


<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <title>Accueil Webmaster et Graphisme</title>
    <link rel="stylesheet" href="css/normalize.css" type="text/css" />
    <link rel="stylesheet" href="css/style.css" type="text/css" />
    <link rel="stylesheet" href="css/blog-mobile_style.css" type="text/css" />



    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Webdesign et Graphisme à Carcassonne et dans l'Aude">

    <link href="https://fonts.googleapis.com/css?family=Pacifico|Playfair+Display:400,400i,700i|Raleway:400,400i,700i" rel="stylesheet">
    <link rel="shortcut icon" href="favicon.ico" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.css" />

    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>
    <script>
        window.addEventListener("load", function(){
    window.cookieconsent.initialise({
      "palette": {
        "popup": {
          "background": "rgba(60,60,60,0.9)"
        },
        "button": {
          "background": "#ffc82e",
          "text": "#404040"
        }
      },
      "content": {
        "message": "En poursuivant votre navigation, vous acceptez le dépôt de cookies tiers destinés à vous proposer des vidéos, des boutons de partage, des remontées de contenus de plateformes sociales",
        "dismiss": "Merci",
        "link": "La gestion des cookies sur notre site",
        "href": "www.hristo-portfolio.alwaysdata.net/mensions.php"
      }
    })});
    </script>
    <!--[if lt IE 9]>
<script
src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js">
</script>
<![endif]-->

</head>

<body>

    <header>

        <nav id="menu" class="js-scrollTo">
            <ul class="flex js-scrollTo">
                <li><a class="js-scrollTo" href="#print">Print</a></li>
                <li><a class="js-scrollTo" href="#web">Web</a></li>
                <li><a class="js-scrollTo" href="#contact">Contact</a></li>
            </ul>
        </nav>

        <section id="montitre">

            <div class="titre1"> Hristo Apostolov</div>
            <div class="titre2"> Webdesign et Graphisme</div>

        </section>



        <section id="entete">

            <div class="titre1"> Hristo Apostolov</div>
            <div class="titre2"> Webdesign et Graphisme</div>

        </section>

        <nav id="menu-mobile">
            <ul>
                <li><a class="js-scrollTo" href="#print">Print</a></li>
                <li><a class="js-scrollTo" href="#web">Web</a></li>
                <li><a class="js-scrollTo" href="#contact">Contact</a></li>
            </ul>
        </nav>
        <!-- fin menu mobile -->



    </header>

    <main class="conteneur">

        <section>
            <article class="print1">
                <h1 id="print"> Mes créations print </h1>
                <article class="flex">

                    <ul class="fancy row flex">
                        <li>
                            <div class="colonne">
                                <a data-fancybox="gallery" href="img/grandformat/carmen.jpg" title="Réalisation maquette affiche pour Opéra Carmen"> <img src="img/carmen.jpg" alt="" /> </a>
                                <a data-fancybox="gallery" href="img/grandformat/magazine.jpg" title="Maquette de magazine VW"> <img src="img/magazine.jpg" alt="Maquette de magazine VW" /> </a>
                            </div>
                        </li>
                        <li>
                            <div class="colonne">
                                <a data-fancybox="gallery" href="img/grandformat/popina;png.png" title="Réalisation d'une maquette menu pour un restaurant"> <img src="img/popina.jpg" alt="Réalisation d'une maquette menu pour un restaurant" /> </a>
                                <a data-fancybox="gallery" href="img/grandformat/agence-eau.jpg" title="Réalisation d'une carte de voeux"> <img src="img/eau.jpg" alt="Réalisation d'une carte de voeux" /> </a>
                            </div>
                        </li>
                        <li>
                            <div class="colonne">
                                <a data-fancybox="gallery" href="img/grandformat/logo.jpg" title="Réalisation de cartes de fidélité"> <img src="img/logo.jpg" alt="Réalisation de cartes de fidélité" /> </a>
                                <a data-fancybox="gallery" href="img/grandformat/kakemono.jpg" title="Réalisation de kakémono"> <img src="img/kakemonofinal.jpg" alt="Réalisation de kakémono" /> </a>
                            </div>
                        </li>
                        <li>
                            <div class="colonne">
                                <a data-fancybox="gallery" href="img/grandformat/invit.jpg" title="Réalisation d'une invitation pour le domaine la Bouysse dans le cadre d'une Journée Portes Ouvertes"> <img src="img/invit.jpg" alt="Réalisation d'une invitation pour le domaine la Bouysse dans le cadre d'une Journée Portes Ouvertes" /> </a>
                                <a data-fancybox="gallery" href="img/grandformat/AVILLA.jpg" title="Création de maquette d'étiquettes en 3 gammes pour une marque d'huile d'olive"> <img src="img/bouteille.jpg" alt="Création de maquette d'étiquettes en 3 gammes pour une marque d'huile d'olive" /> </a>
                            </div>
                        </li>
                        <li>
                            <div class="colonne">
                                <a data-fancybox="gallery" href="img/grandformat/flyermijane.jpg" title="Réalisation d'un flyer pour Château La Mijane"> <img src="img/mijane.jpg" alt="Réalisation d'un flyer pour Château La Mijane" /> </a>
                                <a data-fancybox="gallery" href="img/grandformat/bouyssegrand.jpg" title="Réalisation d'une invitation aux Journées Portes Ouvertes pour le domaine la Bouysse"> <img src="img/bouysse.jpg" alt="Réalisation d'une invitation aux Journées Portes Ouvertes pour le domaine la Bouysse" /> </a>
                            </div>
                        </li>
                    </ul>

                </article>
            </article>




        </section>
        <section id="tri">
            <article class="print">
                <h1 id="web"> Mes créations web </h1>
                <div class="flex">

                    <ul class="listProjet">
                        <li><a href='index.php#tri'>TOUS LES PROJETS</a></li>
                        <?php
      $recherche_villes = array();
      foreach($projets as $k => $annonce){
        if(!in_array($annonce['type'], $recherche_villes)){
          $recherche_villes[] = $annonce['type'];
        }
      }
      sort($recherche_villes);
      foreach($recherche_villes as $k => $type){
        echo "<li><a href='?type=$type#tri'>$type</a></li>";
      }
      ?>
                    </ul>

                </div>
                <article class="flex">

                    <div class="row flex">






                        <?php if(count($resultats)==0) {
      echo "Désolé, aucun projet ne correspond à votre recherche.";
    }else {
      foreach($resultats as $k => $annonce){
    ?>


                        <div class="colonne3">

                            <?php 
          if($annonce['type']!=='WEBDESIGN'){
                   echo "<img src=img/".$annonce['img']." class='box' />"; 
              
          }
         
        ?>

                            <div class="box1">
                                <div class="box2"><a href="<?php echo $annonce['lien']?>">

                                        <?php 
        if(isset($annonce['outil'])){
            echo $annonce['outil'];
        }else{
            
           }
        ?>
                                    </a>
                                </div>
                            </div>


                            <?php 
          if($annonce['type']=='WEBDESIGN'){
                   echo "<img src=img/".$annonce['imge']." class='box' />"; 
              
    
        ?>

                            <div class="box1">
                                <div class="box2"><a href="<?php echo $annonce['lien']?>">

                                        <?php 
        if(isset($annonce['outil'])){
            echo $annonce['outil'];
        }else{
           }echo "<a data-fancybox='gallery' href=img/".$annonce['img']." title='Réalisation maquette de site'><img src=img/".$annonce['img']."alt=''/></a>";
                    }
         
        ?>
                                    </a>
                                </div>
                            </div>



                            <div style="clear:left;"></div>
                        </div>


                        <?php
      }
    }
    ?>


                    </div>




                </article>
            </article>

        </section>


    </main>


    <section>



        <div class="telephone">
            <!--
                <i class="fas fa-mobile-alt"><span>06 22 91 42 07</span></i>
                <i class="far fa-envelope-open"><span> hristo.apostolov@yahoo.com</span></i>
-->


            <div id="contact">
                <h2 class="contacteznous">CONTACT</h2>
                <div class="donnees">
                    <div>
                        <div class="formulaire">
                            <form method="post" action="index.php">
                                <div class="form flex">
                                    <div class="bloc">
                                        <input type="checkbox" id="identite" name="choix1" value="1" />
                                        <label for="identite">Identité visuelle</label>
                                    </div>
                                    <div class="bloc">
                                        <input type="checkbox" id="print" name="choix2" value="2" />
                                        <label for="print">Print</label>
                                    </div>
                                    <div class="bloc">
                                        <input type="checkbox" id="web" name="choix3" value="3" />
                                        <label for="web">Web</label>
                                    </div>
                                    <div class="bloc">
                                        <input type="checkbox" id="devis" name="choix5" value="5" />
                                        <label for="devis">Devis</label>
                                    </div>
                                    <div class="bloc">
                                        <input type="checkbox" id="autre" name="choix6" value="6" />
                                        <label for="autre">Autre</label>
                                    </div>


                                </div>

                                <fieldset>
                                    <div class="centerBloc">
                                        <div class="champSize">
                                            <p>
                                                <!--Texte (une ligne simle)-->
                                                <label for="nom_agent"></label>
                                                <input type="text" name="nom" id="nom" placeholder="Nom/Raison sociale" required="name" />

                                            </p>
                                            <p>
                                                <!--Email (obligatoire)-->
                                                <label for="telephone"></label>
                                                <input type="telephone" id="telephone" name="telephone" placeholder="Téléphone" required />
                                            </p>
                                        </div>
                                        <div class="champSize">
                                            <p>
                                                <!--Email (obligatoire)-->
                                                <label for="mail"></label>
                                                <input type="mail" id="mail" name="mail" placeholder="Mail" required />

                                            </p>

                                            <label for="message"></label>
                                            <textarea name="message" id="message" placeholder="Votre message"></textarea>
                                            <p class="rgpd">Ce site est conforme au RGPD (Règlement Général sur la Protection des Données) RÈGLEMENT du PARLEMENT EUROPÉEN (UE) 2016/679 et du CONSEIL de l'EUROPE du 27 avril 2016 relatif à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données.</p>

                                        </div>
                                    </div>
                                    <p class="bouton">
                                        <input type="submit" name="send" value="Envoyer" />
                                    </p>

                                </fieldset>



                                <?php
if (isset($_POST['send'])) {
	$to = "hristo.apostolov@yahoo.com";
	$from=$_POST['mail'];
	$qui=$_POST['nom'];
	$tel=$_POST['telephone'];
	$message = isset($_POST['message']) ? htmlspecialchars(trim($_POST['message'])) : '';
	$objet =isset($_POST['choix1']) ? 'Identité ' : '';
	$objet .=isset($_POST['choix2']) ? 'Print ' : '';
	$objet .=isset($_POST['choix3']) ? 'Web ' : '';
	$objet .=isset($_POST['choix5']) ? 'Devis ' : '';
	$objet .=isset($_POST['choix6']) ? 'Autre ' : '';
	$message = "Monsieur $qui vous a écrit le message suivant : $message. Son numéro de téléphone est : $tel.";
	if ($objet != '') $message .= " L'objet de sa demande consiste en $objet ";

	mail($to,$objet,$message,$from);
}
?>

                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <footer>
        <div class="flex conteneur">
            <div class="mentions">
                <p class="LogoFooter"> &copy; Hristo Apostolov Créations </p>
                <p class="telFooter">06 22 91 42 07 </p>
            </div>
            <div class="credits">
                <nav id="pied">
                    <ul class="flex">
                        <li><a class="js-scrollTo" href="#print">Print</a></li>
                        <li><a class="js-scrollTo" href="#web">Web</a></li>
                        <li><a class="js-scrollTo" href="#contact">Contact</a></li>
                        <li><a class="js-scrollTo" href="mensions.php">Mensions Légales</a></li>
                        <li>
                            <a href="https://www.instagram.com/designedbyhristo"><i class="fab fa-instagram"></i></a></li>

                        <li> <a href="https://www.linkedin.com/in/hristo-apostolov-92aa8929/"><i class="fab fa-linkedin-in"></i></a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </footer>

    <!-- DEBUT JAVA SCRIPT -->

    <!-- Inclusion bibliothèque Jquery -->
    <script src="js/jquery-1.12.4.min.js"></script>
    <!-- Inclusion feuille de scrpit perso -->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.js">
    </script>

    <script type="text/javascript">
        $(function() {
            $(window).scroll(function() {
                var scroll = $(window).scrollTop();
                if (scroll >= 10) {
                    $(".fondmenu").addClass("fixe");

                } else {
                    $(".fondmenu").removeClass("fixe");

                }

            });

            $('.js-scrollTo').on('click', function() { // Au clic sur un élément
                var page = $(this).attr('href'); // Page cible
                var speed = 750; // Durée de l'animation (en ms)
                $('html, body').animate({
                    scrollTop: $(page).offset().top
                }, speed); // Go
                return false;
            });
        });

    </script>

    <!-- FIN JAVA SCRIPT -->
    <script src="js/zone_script.js" type="text/javascript"></script>

</body>

</html>
