<?php
    session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <link type="text/css" rel="style" href="css/style.css">
    </head>
    <body>
       <main class="fond" style="background-color: #1c9fcb;height: 100vh;margin: 0;">
        <?php
            if(!isset($_POST["identifiant"],$_POST["mdp"])){
        ?>
        <div class="block" style="margin: 0 auto;text-align: center;padding-top: 50vh;">
       <h1 class="titre">Identification requise avant d'accéder au site</h1>
       <form action="index.php" method="post">
           <label for="identifiant" style="
    margin-right: 20px;
">Identifiant</label>
           <input type="text" id="identifiant" name="identifiant">
           <br>
           <label for="mdp">Mot de passe</label>
           <input type="password" id="mdp" name="mdp">
           <br>
           <button type="submit" style="
    margin-top: 20px;
">Valider</button>
       </form>
       <?php
            }else{
                
            require("connectPDO.php");
                
$sql = "SELECT * FROM users WHERE identifiant = :identifiant";

$requetePreparee = $bdd->prepare($sql);

$requetePreparee->bindParam(':identifiant', $_POST['identifiant'], PDO::PARAM_STR);

$requetePreparee->execute();
                
$ligne = $requetePreparee->fetch();
    
        
                $mdp = $ligne['mdp'];
                               
                if($_POST["mdp"]==$mdp && $ligne){
                    echo "<img src='url(../img)";
                    echo "<div class='fondbleu'style='margin: 0 auto;text-align: center;padding-top: 50vh;'>";
                    echo("<span id='welcome'>Bienvenue ".$_POST["identifiant"]."</span>");
                    $_SESSION['identifiant']=$_POST["identifiant"];
                    echo("<p><a href='index.html'>Aller à l'administration du site</a></p>");
                        echo"</div>";
                }else{
                    echo"<div class='fondbleu'style='margin: 0 auto;text-align: center;padding-top: 50vh;'>";
                    echo("<p>Votre identifiant ou mot de passe est erroné. Veuillez réessayer.</p>");
                    echo("<p><a href='index.php'>Ressayer</a></p>");
                     echo"</div>";
                }
            } 
       ?>
       </div>
       </main>
    </body>
</html>