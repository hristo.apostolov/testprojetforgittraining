    <?php
        include("config.php");

        // Essayer d'executer le code entre les deux { }
        try
        {
            // Créer un objet de class PDO qui se connecte à la bdd
            // Il sait faire des trucs avec les bdd
            // Il peut preparer des requetes, executer des requetes, etc.
            $bdd = new PDO('mysql:host='.$adresseServeur.';dbname='.$nomBaseDeDonnees.';charset=utf8', $nomUtilisateur, $motDePasse);
            // On règle l'attribut "mode d'erreur' sur 'Afficher tout les problèmes
            // Les problèmes (erreurs) seront accessible via l'objet PDOException
            $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        //  Le code à executer si le try a échoué
        //  La variable $e sera accessible dans le catch, c'est un objet de type PDOException qui contiendra les informations sur les erreurs
        catch (PDOException $e)
        {
            // Met fin à l'execution du code php (va mourrir php !)
            // Envoi un message d'erreur, récupéré depuis l'objet $e via la méthode getMessage() qui appartient donc à la Class PDOException
            die('Erreur : ' . $e->getMessage());
        }
    ?>