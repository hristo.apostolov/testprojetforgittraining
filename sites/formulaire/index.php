<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="style.css" type="text/css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">


	<title>Document</title>
</head>

<body>
	<div id="contact">
		<h2 class="contacteznous"> CONTACTEZ NOUS !</h2>
		<div class="donnees">
			<img src="img/equipe.png" />
			<div>
				<p class="titre_contact"> Vous souhaitez nous contacter pour</p>
				<div class="formulaire">

					<form method="post" action="index.php">
						<div class="flex form">
							<div>
								<div class="bloc">
									<input type="checkbox" id="identite" name="choix1" value="1" />
									<label for="identite">Identité visuelle</label>
								</div>
								<div class="bloc">
									<input type="checkbox" id="print" name="choix2" value="2" />
									<label for="print">Print</label>
								</div>
								<div class="bloc">
									<input type="checkbox" id="web" name="choix3" value="3" />
									<label for="web">Web</label>
								</div>
								<div class="bloc">
									<input type="checkbox" id="motion" name="choix4" value="4" />
									<label for="motion">Motion design</label>
								</div>
							</div>

							<div class="choix2">
								<div class="bloc">
									<input type="checkbox" id="devis" name="choix5" value="5" />
									<label for="devis">Devis</label>
								</div>
								<div class="bloc">
									<input type="checkbox" id="autre" name="choix6" value="6" />
									<label for="autre">Autre</label>
								</div>
							</div>
						</div>




						<fieldset>

							<p>
								<!--Texte (une ligne simle)-->
								<label for="nom_agent"></label>
								<input type="text" name="nom" id="nom" placeholder="Nom/Raison social" required="name" />
							</p>

							<p>
								<!--Email (obligatoire)-->
								<label for="telephone"></label>
								<input type="telephone" id="telephone" name="telephone" placeholder="Téléphone" required />
							</p>

							<p>
								<!--Email (obligatoire)-->
								<label for="mail"></label>
								<input type="mail" id="mail" name="mail" placeholder="Mail" required />
							</p>

							<p>
								<label for="message"></label>
								<textarea name="message" id="message" placeholder="Votre message"></textarea>
							</p>
							<p class="bouton">
								<input type="submit" name="send" value="Envoyer" />
							</p>
						</fieldset>
					</form>


					<?php 
if (isset($_POST['send'])) {
	$to = "adoptezunwebmaster2019@gmail.com";
	$from=$_POST['mail'];
	$qui=$_POST['nom']; 
	$tel=$_POST['telephone'];
	$message = isset($_POST['message']) ? htmlspecialchars(trim($_POST['message'])) : '';
	$objet =isset($_POST['choix1']) ? 'Identité ' : '';
	$objet .=isset($_POST['choix2']) ? 'Print ' : '';
	$objet .=isset($_POST['choix3']) ? 'Web ' : '';
	$objet .=isset($_POST['choix4']) ? 'Motion Design ' : '';
	$objet .=isset($_POST['choix5']) ? 'Devis ' : '';
	$objet .=isset($_POST['choix6']) ? 'Autre ' : '';
	$message = "Monsieur $qui vous a écrit le message suivant : $message. Son numéro de téléphone est : $tel.";
	if ($objet != '') $message .= " L'objet de sa demande consiste en $objet ";
	mail($to,$objet,$message,$from);
}

//if(isset($_POST[$from]) && isset($_POST[$qui]) && isset($_POST[$tel]) && isset($_POST[$objet])){
//	echo "Message de".$_POST[$qui]." donc le mail est".$_POST[$from]." avec numéro de tél:".$_POST[$tel]." a écrit :".$_POST['message'];
//}else{
//	
//}


?>

				</div>
				</div>
			</div>
			<div class="boxData flex">
				<div class="position1">
					<i class="fas fa-map-marker-alt"></i>
					<div class="textInfo">150 Rue Louis Nicolas Vauquelin <br>31 100 TOULOUSE <br>France</div>
				</div>
				<div class="position2">
					<i class="fas fa-phone"></i>
					<div class="textInfo1">07 77 77 77 07</div>
				</div>
				<div class="position3">
					<i class="fas fa-envelope"></i>
					<div class="textInfo2">fifteen@fifteen.fr</div>
				</div>
			</div>

		</div>
		<div class="fondvert flex">
			<div class="carrinfo flex">
				<div class="infoBox flex">
					<p class="textBasleft">2019 - Fifteen</p>
					<img src="img/logoblanc.png" />
				</div>
				<div class="icones flex">
					<img href="#" class="social" src="img/phonefooter.png" />
					<img class="social" src="img/mailfooter.png" />
					<img class="social" src="img/instafooter.png" />
					<img class="social" src="img/fbfooter.png" />

				</div>
			</div>
		</div>
		<script src="script.js"></script>

</body>





</html>
