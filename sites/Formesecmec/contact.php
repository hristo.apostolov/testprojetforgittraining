<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Formation Sécrétaire Médical(e) - Perpignan" />
    <title>Formation secrétaire médical(e) - école Vidal Perpignan</title>
    <link rel="stylesheet" href="css/normalize.css" />
    <link rel="stylesheet" href="css/style.css" />
    <link rel="shortcut icon" href="favicon.ico" />
    <link rel="stylesheet" href="css/owl.carousel.min.css" />

    <!--Google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans|Roboto" rel="stylesheet">


    <!--Fontawosome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">


    <!--[if lt IE 9]>
    <script
src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js">
</script>
    <![endif]-->
</head>

<body>
    <header>

        <div class="fondmenu js-scrollTo">
            <div class="fondmenu js-scrollTo">
                <nav class="conteneur flex">
                    <div class="contourlogo">
                        <div class="flex">
                            <img class="vidalogo" src="img/vidal.png" />
                            <div>
                                <a href="index.html"> <img src="img/logo-.png" /></a>
                                <p class="vidal"> Partenaire Vidal Formation </p>
                            </div>
                        </div>
                    </div>
                    <ul class="flex menu">
                        <a href="index.html">
                            <li>Accueil</li>
                        </a>
                        <a href="ecole.html">
                            <li>L'école</li>
                        </a>
                        <a href="lesformations.html">
                            <li>Les formations</li>
                        </a>
                        <a href="admission.html">
                            <li>les admissions</li>
                        </a>
                        <a href="professionnels.html">
                            <li>Espace Entreprise</li>
                        </a>
                        <a href="contact.html">
                            <li>Contact</li>
                        </a>
                    </ul>
                </nav>
            </div>

        </div>

    </header>
    <div class="citation">

        <div class="blockcitation">

            <p>Nous retrouver</p>
        </div>
    </div>


    <h3 class="titrecontact conteneur">L'adresse du centre </h3>


    <main class="contactbloc">
        <div class="contact">
            <iframe class="map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d93831.08914980269!2d2.8344616902838484!3d42.69902971435997!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12b06e4e80fd88fd%3A0x1c83306520f2dd4f!2z0J_QtdGA0L_QuNC90Y_QvQ!5e0!3m2!1sbg!2sfr!4v1553706661747" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>

        <div class="blocinfo">


            <div class="tel">
                <i class="fas fa-phone">
                    09 61 63 50 32 / 06 13 15 54 93</i>
                <i class="fas fa-envelope-open-text">
                    formsecmed@gmail.com</i>
            </div>


            <div id="contactform" class="write">
                <div class="formulaire">
                    <form action="php/insertion_agent.php" method="post">
                        <p>
                            <!--Liste déroulante-->
                            <label for="civilite">Civilité</label>
                            <select id="civilite">
                                <option value="Mme" selected="selected">Mme</option>
                                <option value="Mlle">Mlle</option>
                                <option value="Mr">Mr</option>
                            </select>
                        </p>

                        <p>
                            <!--Texte (une ligne simle)-->
                            <label for="nom_agent">Votre nom :</label>
                            <input type="text" name="nom" id="nom" placeholder="Votre Nom" required />
                        </p>

                        <p>
                            <!--Email (obligatoire)-->
                            <label for="email">Email :</label>
                            <input type="email" id="email" name="email" placeholder="votre@adresse.fr" required />
                        </p>

                        <p>
                            <!--Téléphone-->
                            <label for="tel">Téléphone :</label>
                            <input type="number" name="tel" id="tel" placeholder="Votre Téléphone" required />
                        </p>

                        <!--Fin section 1-->
                        <!--Section 2-->
                        <legend></legend>
                        <!-- Cases à cocher (plusieurs choix possibles)-->

                        <!-- Zone de texte (plusieurs lignes)-->
                        <p>
                            <label for="message">Message</label>
                            <br />
                            <textarea name="message" rows="10" cols="35" id="message" placeholder="Votre message"></textarea>
                        </p>

                        <!--Fin section 2-->

                        <input type="submit" value="Valider" />
                        <input type="reset" value="Effacer" />



                      <?php
                        if (isset($_POST['send'])) {
                        	$to = "hristo.apostolov@yahoo.com";
                        	$from=$_POST['email'];
                        	$qui=$_POST['nom'];
                        	$tel=$_POST['tel'];
                        	$message = isset($_POST['message']) ? htmlspecialchars(trim($_POST['message'])) : '';
                        	$message = "Monsieur/Madame $qui vous a écrit le message suivant : $message. Son numéro de téléphone est : $tel.";
                        	mail($to,$objet,$message,$from);
                        }
                        ?>
                    </form>
                </div>

            </div>

        </div>





    </main>

    <footer>

        <div class="bloc5 flex">

            <div class="colonne1">
                <ul class="footermnu">
                    <li><a href="index.html">Accueil</a> </li>
                    <li><a href="ecole.html">l'école </a> </li>
                    <li><a href="lesformations.html">Les formations</a> </li>
                    <li><a href="admission.html">les admissions</a> </li>
                    <li><a href="professionnels.html">Espace Entreprise</a> </li>
                    <li class="dernier"><a href="contact.html">Contact</a> </li>

                </ul>
            </div>
            <div class="colonne2">
                <ul class="footermnu">
                    <li class="fonta"><span>FORMSECMEC <br> Perpignan 66000
                        </span></li>
                    <li class="fonta"><span>09 61 63 50 32 <br> 06 13 15 54 93</span></li>



                </ul>
            </div>


            <div class="reseaux">
                <div class="icones flex">
                    <a href="https://www.facebook.com/Formsecmed-2347794528836481">
                        <div class="rondgris"><img src="img/facebook-icone.png" /></div>
                    </a>
                </div>

            </div>

        </div>


    </footer>


    <!-- DEBUT JAVASCRIPT-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <!-- Bibliotheque jQuery -->

    <script src="http://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-
ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
    <!--ici la bibliothèque est liée en lien direct, je peux aussi télécharger le dossier et le copier dans mon fichier js et le lier ici-->

    <!-- Fancybox -->
    <script src="js/owl.carousel.min.js"></script>
    <!-- Options fancybox -->
    <script type="text/javascript">
        $(function() {
            $('.owl-carousel').owlCarousel({
                slideBy: 1, // defilement 1 par 1
                autoplay: true, // defilement automatique
                autoplayTimeout: 2000, // au bout de 10s
                autoplayHoverPause: true, // stoppe défilement au survol
                loop: true, // boucle
                navText: ["<i class='fas fa-angle-left'></i>", "<i class='fas fa-angle-right'></i>"], // apparence des fleches
                responsive: {
                    0: {
                        items: 1,
                        nav: false,
                        margin: 0,
                        dots: true
                    },
                    500: {
                        items: 2,
                        margin: 10,
                        nav: false,
                        dots: true
                    },
                    900: {
                        items: 2,
                        dots: false,
                        nav: true,
                        margin: 20
                    }
                }
            });

            // <!-- Défilement animé -menu qui réapparaît  -->
            // $(window).scroll(function() {
            //var scroll = $(window).scrollTop();
            //if (scroll >= 100) {
            //$(".fondmenu").addClass("fixe");
            // $("body").css("padding-top", "65px");

            //} else {
            // $(".fondmenu").removeClass("fixe");
            // $("body").css("padding-top", "0");
            // }


            $(window).scroll(function() {
                var scroll = $(window).scrollTop();
                if (scroll >= 10) {
                    $(".fondmenu").addClass("fixe");

                } else {
                    $(".fondmenu").removeClass("fixe");

                }

            });

            $('.js-scrollTo').on('click', function() { // Au clic sur un élément
                var page = $(this).attr('href'); // Page cible
                var speed = 750; // Durée de l'animation (en ms)
                $('html, body').animate({
                    scrollTop: $(page).offset().top
                }, speed); // Go
                return false;
            });
        });
    </script>
</body>

</html>
